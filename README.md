This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) template.

## Technologies
Project is created with:
* Node
* Npm
* React: 16.3.1
* Redux
* React-bootstrap
* React-router

## Setup
Configure the environment variables:

```bash
$ cd <project-path> 
# Replace `<project-path>` with path of the project.
$ cp .env.example .env
```

Modify the env variables, ex:

```
REACT_APP_ENV=development
REACT_APP_API_URL=https://localhost:8000
REACT_APP_SEARCH=$REACT_APP_API_URL/api/search/
REACT_APP_STORAGE=https://my-storage.com/images/
```

To run this project, install dependencies using npm:

```
$ npm install
$ npm start
```

Open [http://localhost:3000](http://localhost:3000) to view in the browser.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.
