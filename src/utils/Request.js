export { default as Request } from './Request';

const request = async (url, options = {}) => {
  let fetchUrl = url;

  if (options.query){
    fetchUrl = new URL(url);
    fetchUrl.search = new URLSearchParams(options.query);
  }

  const headers = {
    'Content-Type': 'application/json',
    'accept': 'application/json',
    ...options.extraHeaders
  };

  const data = {
    method: options.method || 'GET',
    body: headers['Content-Type'] === 'application/json' ? JSON.stringify(options.body) : options.body
  };

  if (!options.noHeaders) data.headers = headers;

  return fetch(fetchUrl, data)
    .then(res => res.json().then(data => {
      return ({status: res.status, data});
    }).catch(error => {
      return ({ status: res.status, success: false, data: error });
    })).then(res => res);
};

export default request;