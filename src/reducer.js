/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux';

/* Import reducers */
import AppReducer from './App/reducer';
import { SearchReducer } from './views/Search';

export default function rootReducers() {
  return combineReducers({
    App: AppReducer,
    Search: SearchReducer
  });
}