const common = {
  baseUrlApi: process.env.REACT_APP_API_URL || 'http://localhost:3000/api',
  apiSearchUrl: process.env.REACT_APP_SEARCH,
  storageUrl: process.env.REACT_APP_STORAGE
};

export default {
  env: process.env.REACT_APP_ENV || 'development',
  ...common
};