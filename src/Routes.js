import React from 'react';
import { Switch } from 'react-router-dom';

import { RouteWithLayout } from './components';
import { Main as MainLayout } from './layouts';
// import { Route } from 'react-router-dom'

import {
  Search as SearchView
} from './views';

const Routes = () => {
  return (
    <Switch>
      <RouteWithLayout
        component={SearchView}
        exact
        layout={MainLayout}
        path="/"
      />
      {/* <Route
        exact
        path="/"
        render={matchProps => (
          <SearchView {...matchProps} />
        )}
      /> */}
    </Switch>
  );
};

export default Routes;
