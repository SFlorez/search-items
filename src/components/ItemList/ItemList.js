import React from 'react';
import PropTypes from 'prop-types';
import { Pagination } from 'react-bootstrap';
import { Item } from '../index';

const ItemList = props => {
  const { 
    items,
    currentPage,
    totalItems,
    totalPages,
    featuredItems = [],
    onClickNexPage,
    onClickPrevPage,
    onClickPage,
    onClickLastPage,
    onClickFirstPage,
    ...rest 
  } = props;

  const renderItems = (items, featured = false) => {
    const listItems = items.map(( item ) =>
      <Item 
        featured={featured} 
        item={item}
        key={`item-${item.id}`}
      />
    );

    return listItems;
  };

  const getPages = (currentPage, totalPages) => {

    var pages = [];

    for (let index = 1; index <= 5 && index <= totalPages; index++) {

      if (currentPage <= 3 ) {
        pages.push(index);
      } else if( currentPage >= totalPages - 5) {
        pages.push(totalPages - ((index - 5) * -1 ));
      } else {
        pages.push( currentPage + (index - 3) );
      }
    }
    
    return pages.map((page) =>
      <Pagination.Item 
        active={currentPage === page} 
        key={`page-${page}`}
        onClick={(event) => onClickPage(event, page)}
      >
        {page}
      </Pagination.Item>
    );
  };

  return (
    <section {...rest}>
      <div className="d-flex justify-content-between pt-3 pb-4 text-app">
        <div>
          <b>Page {currentPage}</b> of <b>{totalItems} results</b>
        </div>
        <div>
          Sorted by:
        </div>
      </div>
      {renderItems(featuredItems, true)}
      {renderItems(items)}

      <div className="d-flex justify-content-end pt-3">
        {(totalPages > 0) &&
          <Pagination>
            <Pagination.First
              disabled={currentPage === 1}
              onClick={onClickFirstPage}
            />
            <Pagination.Prev
              disabled={currentPage === 1}
              onClick={onClickPrevPage}
            />

            {getPages(currentPage, totalPages)}

            <Pagination.Next
              disabled={currentPage === totalPages}
              onClick={onClickNexPage}
            />
            <Pagination.Last
              disabled={currentPage === totalPages}
              onClick={onClickLastPage}
            />
          </Pagination>
        }
      </div>
    </section>
  );
};

ItemList.propTypes = {
  currentPage: PropTypes.number,
  featuredItems: PropTypes.array,
  items: PropTypes.array,
  onClickFirstPage: PropTypes.func,
  onClickLastPage: PropTypes.func,
  onClickNexPage: PropTypes.func,
  onClickPage: PropTypes.func,
  onClickPrevPage: PropTypes.func,
  totalItems: PropTypes.number,
  totalPages: PropTypes.number
};

export default ItemList;