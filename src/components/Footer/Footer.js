import React from 'react';
// import { Link as RouterLink } from 'react-router-dom';

const Footer = props => {
  const { ...rest } = props;

  return (
    <footer {...rest} />
  );
};

export default Footer;