import React from 'react';
import PropTypes from 'prop-types';
import { 
  InputGroup,
  FormControl,
  Container,
  Row,
  Col,
  Nav,
  Dropdown
} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

import './styles.css';

const Header = props => {
  const { 
    searchValue,
    onChangeSearch,
    onSubmit,
    ...rest
  } = props;

  return (
    <header
      className="search-header mb-5"
      {...rest}
    >
      <Container>
        <Row className="justify-content-md-center pb-2 pt-3">
          <Col
            className="d-flex justify-content-center align-items-center"
            md="8"
          >
            <span className="text-header text-white mr-2">Find CE for a </span> 
            <Dropdown className="mr-2">
              <Dropdown.Toggle
                size="lg"
                variant="outline-light"
              >
                Florida
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item disabled>Option 2</Dropdown.Item>
                <Dropdown.Item disabled>Option 3</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown>
              <Dropdown.Toggle
                size="lg"
                variant="outline-light"
              >
                Medical Doctor
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item disabled>OPTION 2</Dropdown.Item>
                <Dropdown.Item disabled>OPTION 3</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Col>
        </Row>
        <Row className="justify-content-md-center">
          <Col md="8">
            <form onSubmit={onSubmit}>
              <InputGroup className="my-3">
                <InputGroup.Prepend>
                  <InputGroup.Text>
                    <FontAwesomeIcon icon={faSearch} />
                  </InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                  aria-describedby="basic-addon1"
                  aria-label="Username"
                  onChange={onChangeSearch}
                  placeholder="Search courses and providers"
                  value={searchValue}
                />
              </InputGroup>
            </form>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Nav
            defaultActiveKey="link-1"
            variant="tabs"
          >
            <Nav.Item>
              <Nav.Link eventKey="link-1">COURSES</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey="link-2">PROVIDERS</Nav.Link>
            </Nav.Item>
          </Nav>
        </Row>
      </Container>
    </header>
  );
};

Header.propTypes = {
  onChangeSearch: PropTypes.func,
  onSubmit: PropTypes.func,
  searchValue: PropTypes.string
};

export default Header;