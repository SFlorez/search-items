import React from 'react';
import PropTypes from 'prop-types';
import { Card, Row, Col, Button, Image } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClock, faLaptop, faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';

import { env } from '../../config';

import './style.css';

const Item = props => {
  const { item, featured = false, ...rest } = props;

  const calculateHours = (components) => {
    var hours = 0;
    components.forEach(component => {
      if(component.profession) hours += component.profession.totalHours;
    });
    return `${hours} Hours`;
  };

  return (
    <Card
      className="item-card mb-3 pt-3 pb-2"
      {...rest}
    >
      <Card.Body>
        <Row>
          {featured &&
            <Col sm="4">
              {item.course.featuredBanner &&
                <Image
                  alt=""
                  fluid
                  src={`${env.storageUrl}/${item.course.featuredBanner}`}
                />
              }
            </Col>
          }
          <Col
            className="d-flex flex-column justify-content-between"
            sm={featured ? 6 : 10}
          >
            <div>
              <h6>
                {item.course.name}
              </h6>
              {featured && 
                <div className="pb-2 mb-1">
                  <span className="featured-bag">FEATURED</span>
                </div>
              }
              {(item.course.provider && item.course.provider.name) &&
                <h6 className="text-provider">{item.course.provider.name}</h6>
              }
            </div>
            <div className="mt-4">
              <span className="mr-2 text-muted text-footer-item">
                <FontAwesomeIcon
                  className="mr-2"
                  icon={faClock}
                />
                { item.course.components ? calculateHours(item.course.components) : '0 Hours'}
              </span>
              <span className="text-muted text-footer-item">
                <FontAwesomeIcon
                  className="mr-2"
                  icon={faLaptop}
                />
                { item.course.deliveryMethod ? item.course.deliveryMethod.description : 'n/n' }
              </span>
            </div>

          </Col>
          <Col
            className="d-flex flex-column justify-content-between align-items-end"
            sm="2"
          >
            <h5 className="text-price">$ {item.price}</h5>
            <div>
              <Button 
                disabled
                variant="outline-secondary"
              >
                <FontAwesomeIcon icon={faExternalLinkAlt} />
              </Button>
            </div>
          </Col>
        </Row>

      </Card.Body>
    </Card>
  );
};

Item.propTypes = {
  featured: PropTypes.bool,
  item: PropTypes.object
};

export default Item;