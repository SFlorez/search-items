import React from 'react';
// import { Link as RouterLink } from 'react-router-dom';
import { Navbar, Nav, NavDropdown, Container, Button } from 'react-bootstrap';
import logo from '../../assets/logo.svg';
import PropTypes from 'prop-types';


const Topbar = props => {
  const { ...rest } = props;

  return (
    <Navbar
      bg="light"
      expand="lg"
      {...rest}
    >
      <Container>
        <Navbar.Brand 
          className="pr-md-5" 
          href="/"
        >
          <img
            alt="React Bootstrap logo"
            className="d-inline-block align-top"
            height="30"
            src={logo}
            width="30"
          />
          Search
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="#">Feacures</Nav.Link>
            <Nav.Link href="#">Plans</Nav.Link>
            <NavDropdown title="Organizations">
              <NavDropdown.Item
                disabled
                href="#"
              >Action</NavDropdown.Item>
              <NavDropdown.Item
                disabled
                href="#"
              >Another action</NavDropdown.Item>
            </NavDropdown>
            <Nav.Link
              active
              href="/"
            >Browse courses</Nav.Link>
            <Nav.Link href="#">Support</Nav.Link>
          </Nav>
          <Button
            className="square-button"
            variant="outline-secondary mr-3"
          >Sign in</Button>
          <Button
            className="square-button"
            variant="success"
          >7 day trial</Button>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

Topbar.propTypes = {
  className: PropTypes.string
};

export default Topbar;
