import React from 'react';
import PropTypes from 'prop-types';

import { Topbar, Footer } from '../../components';

import './style.css';

const Main = props => {
  const { children } = props;

  return (
    <div className={'root'}>
      <Topbar/>
      <main className={'main-content'}>
        {children}
        <Footer />
      </main>
    </div>
  );
};

Main.propTypes = {
  children: PropTypes.node
};

export default Main;
