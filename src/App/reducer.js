import { DEFAULT_ACTION } from './constants';

export const initialState = {
  count: 0
};

function appReducer (state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    default:
      return state;
  }
}

export default appReducer;