import { initialState } from './reducer';
 
const appSelector = state => state.App || initialState;

export default appSelector;