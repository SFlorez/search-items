import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Routes from './Routes';
import store from './store';


export default class App extends Component {
  render() {
    return (
      <React.StrictMode>
        <Provider store={store}>
          <Router>
            <Routes />
          </Router>
        </Provider>
      </React.StrictMode>
    );
  }
}
