import { request } from '../../utils';
import { env } from '../../config';
import { 
  DEFAULT_ACTION,
  LOAD_ITEMS,
  LOAD_ITEMS_SUCCESS,
  LOAD_FEATURED_SUCCESS,
  LOAD_ITEMS_ERROR
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function loadItemsSuccess(items, totalItems, totalPages) {
  // Calculate pages number
  return {
    type: LOAD_ITEMS_SUCCESS,
    payload: { items, totalItems, totalPages }
  };
}

export function loadFeaturedSuccess(featuredItems) {
  return {
    type: LOAD_FEATURED_SUCCESS,
    payload: { featuredItems }
  };
}

export function loadItemsFailure(error, response) {
  console.error(response);
  return {
    type: LOAD_ITEMS_ERROR,
    payload: { error, response }
  };
}

function calculatePages (totalItems, pageSize) {
  return Math.ceil(totalItems / pageSize);
}

export function loadItems(options = {}) {
  return dispatch => {
    dispatch({ type: LOAD_ITEMS });
    return request(env.apiSearchUrl, options)
      .then(async json => {
        if (json && json.status === 200 && json.data && Array.isArray(json.data.items)) {

          if (options.query && options.query.isFeatured) dispatch(loadFeaturedSuccess(json.data.items));
          else dispatch(loadItemsSuccess(
            json.data.items, 
            json.data.totalItems,
            calculatePages(json.data.totalItems, options.query.pageSize))
          );

        } else dispatch(loadItemsFailure('Error on request', json));

        return json;
      })
      .catch(error => {
        console.log(error);
        dispatch(loadItemsFailure('Error on request', error));
        return false;
      });
  };
}
