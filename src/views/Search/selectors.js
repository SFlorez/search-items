import { createSelector } from 'reselect';
import { initialState } from './reducer';
 
const searchSelector = () => state => state.Search || initialState;

const makeSelectSearch = () => createSelector(
  searchSelector(),
  (substate) => substate
);

export default makeSelectSearch;
export {
  searchSelector
};