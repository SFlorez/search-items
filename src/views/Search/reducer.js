import { 
  DEFAULT_ACTION, 
  LOAD_ITEMS, 
  LOAD_ITEMS_SUCCESS,
  LOAD_FEATURED_SUCCESS, 
  LOAD_ITEMS_ERROR 
} from './constants';

export const initialState = {
  loading: false,
  error: false,
  response: null,
  items: [],
  featuredItems: [],
  totalItems: 0,
  totalPages: 0
};

function searchReducer (state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case LOAD_ITEMS:
      return Object.assign({}, state, {
        loading: true
      });
    case LOAD_ITEMS_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        items: action.payload.items,
        totalItems: action.payload.totalItems,
        totalPages: action.payload.totalPages
      });
    case LOAD_FEATURED_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        featuredItems: action.payload.featuredItems
      });
    case LOAD_ITEMS_ERROR:
      return Object.assign({}, state, {
        loading: false,
        error: true,
        response: action.payload.response
      });
    default:
      return state;
  }
}

export default searchReducer;