export const DEFAULT_ACTION = 'search-app/search/DEFAULT_ACTION';
export const LOAD_ITEMS = 'search-app/App/LOAD_ITEMS';
export const LOAD_ITEMS_SUCCESS = 'search-app/App/LOAD_ITEMS_SUCCESS';
export const LOAD_FEATURED_SUCCESS = 'search-app/App/LOAD_FEATURED_SUCCESS';
export const LOAD_ITEMS_ERROR = 'search-app/App/LOAD_ITEMS_ERROR';
