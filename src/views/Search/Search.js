import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import {
  Container,
  Row,
  Col,
  Card
} from 'react-bootstrap';
import { ItemList, Header } from '../../components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilter } from '@fortawesome/free-solid-svg-icons';

import makeSelectSearch from './selectors';
import * as searchActions from './actions';

import './style.css';

class Search extends Component {

  constructor(props) {
    super(props);
    const paramsString = this.props.location.search;
    const searchParams =  new URLSearchParams(paramsString);
    const searchQuery = searchParams.get('search');
    const pageQuery = parseInt(searchParams.get('page'));

    this.state = { 
      page: pageQuery || 1,
      search: searchQuery || ''
    };
  }

  componentDidMount() {
    this.getItems();
  }

  getItems = () => {
    const { loadItems } = this.props;
    const { page, search } = this.state;

    const query = { 
      expand: 'totalItems', 
      pageIndex: page, 
      pageSize: 7, 
      sortField: 'RELEVANCE', 
      profession: 36, 
      courseType: 'CD_ANYTIME' 
    };
    // Get featured items
    loadItems({ query: { ...query, isFeatured: true }});

    // Get items
    const itemsOptions = { query: { ...query, pageSize: 10, state: 'FL' }};
    if (search) itemsOptions.query.term = search;
    loadItems(itemsOptions);

    this.scrollToTop();
  }

  handleNextPage = (event) => {
    const { totalPages, history } = this.props;
    const { page } = this.state;

    if (page < totalPages) {
      const nextPage = page + 1;
      history.push(`?search=${this.state.search}&page=${nextPage}`);
      this.setState({ page: nextPage }, () => this.getItems());
    }
    event.preventDefault();
  }

  handlePrevPage = (event) => {
    const { page, search } = this.state;

    if (page > 1) {
      const prevPage = page - 1;
      this.props.history.push(`?search=${search}&page=${prevPage}`);
      this.setState({ page: prevPage }, () => this.getItems());
    }
    event.preventDefault();
  }

  handleGoToPage = (event, goPage) => {
    const { page, search } = this.state;

    if (goPage !== page) {
      this.props.history.push(`?search=${search}&page=${goPage}`);
      this.setState({ page: goPage }, () => this.getItems()); 
    }
    event.preventDefault();
  }

  handleGoLastPage = (event) => {
    const { totalPages, history } = this.props;
    const { page, search } = this.state;
    if (page !== totalPages) {
      history.push(`?search=${search}&page=${totalPages}`);
      this.setState({ page: totalPages }, () => this.getItems());
    }
    event.preventDefault();
  }

  handleGoFirstPage = (event) => {
    const { totalPages, history } = this.props;
    const { page, search } = this.state;
    if (page !== 1 && totalPages > 0) {
      history.push(`?search=${search}&page=${1}`);
      this.setState({ page: 1 }, () => this.getItems());
    }
    event.preventDefault();
  }

  handleChangeSearch = (event) => {
    var value = event.target.value;
    this.setState({ search: value });
    event.preventDefault();
  }

  handleSubmit = (event) => {
    this.props.history.push(`?search=${this.state.search}`);
    this.setState({ page: 1 }, this.getItems());
    event.preventDefault();
  }

  scrollToTop() {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  }

  render() {
    const { items, featuredItems, totalItems, totalPages } = this.props;
    const { page, search } = this.state;

    return (
      <div className="search">
        <Header 
          onChangeSearch={this.handleChangeSearch}
          onSubmit={this.handleSubmit}
          searchValue={search}
        />
        <Container className="pt-2 px-md-4">
          <Row>
            <Col 
              className="pr-md-2"
              md="3" 
            >
              <Card className="app-card">
                <Card.Header className="text-center">
                  <span className="text-sm text-app">
                    <FontAwesomeIcon  
                      className="mr-1 text-muted" 
                      icon={faFilter}
                    /> 
                    FILTER COURSE RESULTS
                  </span> 
                </Card.Header>
                <Card.Body className="text-center">
                  <span className="text-sm text-muted">Comming soon</span>
                </Card.Body>
              </Card>
            </Col>
            <Col 
              className="pl-md-2" 
              md="9"
            >
              <Card className="app-card">
                <Card.Body>
                  <ItemList 
                    currentPage={page}
                    featuredItems={featuredItems} 
                    items={items} 
                    onClickFirstPage={this.handleGoFirstPage}
                    onClickLastPage={this.handleGoLastPage}
                    onClickNexPage={this.handleNextPage}
                    onClickPage={this.handleGoToPage}
                    onClickPrevPage={this.handlePrevPage}
                    totalItems={totalItems}
                    totalPages={totalPages}
                  />
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

Search.propTypes = {
  featuredItems: PropTypes.array,
  history: PropTypes.object,
  items: PropTypes.array,
  loadItems: PropTypes.func,
  location: PropTypes.object,
  totalItems: PropTypes.number,
  totalPages: PropTypes.number
};

const mapStateToProps = makeSelectSearch();

function mapDispatchToProps(dispatch) {
  const actions = bindActionCreators(searchActions, dispatch);
  return {
    dispatch,
    ...actions
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Search);
